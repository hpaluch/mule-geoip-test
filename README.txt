This simple mule 3.4.0 project process list of IPv4 addresses from file
 and resolves Geolocation using 
 GeoIP public service (http://www.webservicex.net/geoipservice.asmx?WSDL)
 and resolves hostname using internal REST service. Results are outputet to file in filesystem.

Note:
 This project requires full JDK Runtime (it uses CXF to call WS,
 which in turn uses javac to generate WS client)

Setup:
Copy src\test\resources\data somewhere to your filesystem.
Specify that directory in src\main\resources\myflow.properties 

Run:
 Choice Run As Mule Application

