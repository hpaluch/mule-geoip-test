package org.bitbucket.hpaluch.mule_geoip_test;

import java.net.InetAddress;
import java.util.Hashtable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/dns-service")
public class DNSService {
	
	static Hashtable<String,String> ip2host = new Hashtable<String,String>();
	
    @GET
    @Produces("text/xml")
    @Path("/getHostByAddr/{ip}") 
	public HostAddrBean getHostByAddr(@PathParam("ip") String ip){
    	if (ip2host.size()>1000){
    		ip2host.clear();
    	}
    	System.err.println("Got DNS request for: "+ip);
    	HostAddrBean hb = new HostAddrBean();
    	hb.setIpAddress(ip);
    	String[] ips = ip.split("\\.");
    	try{
    	if (ips.length==4){
    		byte[] bips = new byte[4];
    		for(int i=0;i<ips.length;i++){
    			bips[i]= (byte)(Integer.parseInt(ips[i]) & 0xff);
    		}
    		String h = ip2host.get(ip);
    		boolean cached = true;
    		if ( h==null){    		
    			InetAddress a = InetAddress.getByAddress(bips);
    			h= a.getCanonicalHostName();
    			ip2host.put(ip,h);
    			cached = false;
    		}
    		hb.setHostname(h);
    		hb.setCached(cached);
    	}    	
		return hb;
    	}catch(Throwable t){
    		throw new RuntimeException(t);
    	}
	}
	
}
