package org.bitbucket.hpaluch.mule_geoip_test;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.mule.api.transformer.DataType;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;
import org.mule.transformer.types.DataTypeFactory;
import org.mule.util.IOUtils;

public class StringLinesToCollectionTransformer extends AbstractTransformer {

	public StringLinesToCollectionTransformer(){
		registerSourceType(DataType.STRING_DATA_TYPE);
        registerSourceType(DataTypeFactory.BYTE_ARRAY);
        registerSourceType(DataTypeFactory.INPUT_STREAM); 		
		setReturnDataType(DataTypeFactory.create(Collection.class));
	}
	
	
	@Override
	protected Object doTransform(Object src, String outputEncoding)
			throws TransformerException {
		Object out = src;
		String in = "";
		
        if (src instanceof byte[])
        {
            in = createStringFromByteArray((byte[]) src, outputEncoding);
        }
        else if (src instanceof InputStream)
        {
            in = createStringFromInputStream((InputStream) src);
        }
        else
        {
            in = (String) src;
        } 
		
		
		if (in!=null){
			String[] lines = in.split("[\\r\\n]+");
			List<String> collectionLines = new ArrayList<String>();
			for(int i=0;i<lines.length;i++){
				lines[i] = lines[i].trim();
				if(lines[i].length()>0){
					collectionLines.add(lines[i]);
				}
			}
			out=collectionLines;
		}		
		return out;
	}
	
    protected String createStringFromByteArray(byte[] bytes, String outputEncoding) throws TransformerException
    {
        try
        {
            return new String(bytes, outputEncoding);
        }
        catch (UnsupportedEncodingException uee)
        {
            throw new TransformerException(this, uee);
        }
    }

    protected String createStringFromInputStream(InputStream input)
    {
        try
        {
            return IOUtils.toString(input);
        }
        finally
        {
            IOUtils.closeQuietly(input);
        }
    } 
}
