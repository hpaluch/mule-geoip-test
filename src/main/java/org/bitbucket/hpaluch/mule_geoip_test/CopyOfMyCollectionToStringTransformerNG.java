package org.bitbucket.hpaluch.mule_geoip_test;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.endpoint.ImmutableEndpoint;
import org.mule.api.lifecycle.InitialisationException;
import org.mule.api.transformer.DataType;
import org.mule.api.transformer.Transformer;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;
import org.mule.transformer.types.DataTypeFactory;

public class CopyOfMyCollectionToStringTransformerNG extends AbstractTransformer {

	public CopyOfMyCollectionToStringTransformerNG(){
		registerSourceType(DataTypeFactory.create(Collection.class));
		setReturnDataType(DataTypeFactory.TEXT_STRING);
	}

	@Override
	protected Object doTransform(Object src, String outputEncoding)
			throws TransformerException {

//		System.err.println("Hello: "+src+" class: "+src.getClass());
		StringBuffer out = new StringBuffer();
		Collection c = (Collection)src;
		 for(Iterator i=c.iterator();i.hasNext();){
			 Object o = i.next();
			 if (out.length()>0){
				 out.append("\r\n");
			 }
			 out.append(o.toString());
		 }
//		 System.out.println("Returning: "+out.toString());
		 return out.toString();
	}
	

}
