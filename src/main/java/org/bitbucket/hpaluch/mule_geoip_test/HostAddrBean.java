package org.bitbucket.hpaluch.mule_geoip_test;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement 
public class HostAddrBean {
	private String ipAddress;
	private String hostname;
	private boolean cached;
	
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public boolean isCached() {
		return cached;
	}
	public void setCached(boolean cached) {
		this.cached = cached;
	}
	
	
	
}
